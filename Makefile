.PHONY: clean docs mod

mod:
	chmod 755 -R .

docs:
	python docs/gen.py

clean:
	rm -rf *.log
	rm -rf data/*regrid*
	rm -rf src/*.pyc
	rm -rf src/*#*
	rm -rf src/*~
	rm -rf tests/*.pyc
	rm -rf *.last
	rm -rf .nfs*
	rm -rf tmp_imagedec_*
	rm -rf tmp_tp2viswt*
	rm -rf TempLattice*
