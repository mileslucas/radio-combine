'''
pipeline.py

Miles Lucas - mdlucas@nrao.edu
'''
import os
import argparse
import numpy as np
from collections import OrderedDict

from simulate import simulate, clean
from combine import feather_comb, sm_clean, jd, tp2v
from compare import compare
import _models as m

def pipeline(project, model_names=m.models.keys(), simdata=True, compare_only=False, save_data=True, parallel=False):
	"""
	Does a full pipeline with given project name`
	
	Parameters
	----------
	project : str
		Project name and folder. Will automatically add to .gitignore
	model_names : list, optional
		The models to run in the pipeline, from _models. Out of {} (default is all)
	simdata : bool, optional
		If True, will simulate the model data (the default is True)
	compare_only : bool, optional
		If True, will only run the comparison scripts. (The default is False)
	save_data : bool, optional
		If True, will save csv and png files from the pipeline (the default is True)
	parallel : bool, optional
		If True, will run the combinations in parallel. (the default is False)
	""".format(m.models.keys())
	# Add to gitignore if not already present
	ignore = project + '/'
	with open('.gitignore', 'r') as gi:
		text = gi.read().strip().split()
	
	with open('.gitignore', 'a') as gi:
		if not ignore in text:
			gi.write(ignore + '\n')
			
	if not isinstance(model_names, list):
			model_names = [model_names,]

	# Check models
	for model in model_names:
		if not model in m.models:
			raise ValueError('{} is not a recognized model'.format(model))

	# Model images
	for model in model_names:
		if model == 'ppd':
			ms = None
			if not compare_only:
				ms = 'data/ppd.int.ms'
				feather_comb('data/ppd', 'data/ppd.int.image', 'data/ppd.sd.image')
				params = m.models['ppd']['params']
				params.update(dict(parallel=parallel))
				sm_clean('data/ppd', 'data/ppd.int.ms', 'data/ppd.sd.image', **params)
				jd('data/ppd', 'data/ppd.int.ms', 'data/ppd.sd.image', **params)
				tp2v('data/ppd', 'data/ppd.int.ms', 'data/ppd.sd.image', **params)

			tests = ['ppd.feather.image', 'ppd.smclean.image', 'ppd.int.image', 'ppd.joint.image',
				'ppd.tp2vis.image']

			comparisons('data', 'ppd', 'ppd.conv', tests)
			comparisons('data', 'ppd', 'ppd.sd.image', tests)
			continue
				
		if simdata and not compare_only:
			simulate(project, model, model, parallel=parallel)

		base = project + '/' + model
		vis = project + '/{}.int.ms'.format(model)
		highres = project + '/{}.int.image'.format(model)
		lowres = project + '/{}.sd.image'.format(model)
		model_name = 'models/{}.conv'.format(model)
		sd_name = '{}.sd.image'.format(model)
		int_name = '{}.int.image'.format(model)
		if not compare_only:
			params = m.models[model]['params']
			params.update(dict(parallel=parallel))
			feather_comb(base, highres, lowres)
			sm_clean(base, vis, lowres, **params)
			jd(base, vis, lowres, **params)
			tp2v(base, vis, lowres, **params)

		feather_name = '{}.feather.image'.format(model)
		sm_name = '{}.smclean.image'.format(model)
		joint_name = '{}.joint.image'.format(model)
		tp2v_name = '{}.tp2vis.image'.format(model)
		tests = [feather_name, sm_name, int_name, joint_name, tp2v_name]
				
		comparisons(project, model, model_name, tests, save=save_data)
		comparisons(project, model, sd_name, tests, save=save_data)
	

def comparisons(project, model, model_name, tests, 
			d_base='docs/static/images/diags/', r_base='docs/static/images/results/', data_base='docs/static/ratios/', chi2_base='docs/static/chi2/', save=True):
	"""
	Runs comparisons for every test image against the model image
	
	Parameters
	----------
	project : str
		The folder containing all the data files
	model : str
		the name of the model reference image
	tests : list
		A list of paths to each test image
	d_base : str, optional
		The base for saving diagnostic images (the default is 'docs/static/images/diags/')
	r_base : str, optional
		The base for saving results images (the default is 'docs/static/images/results/')
	data_base : str, optional
		The base for saving ratio csv (the default is 'docs/static/ratios/')
	chi2_base : str, optional
		The base for saving chi2 csv (the default is 'docs/static/chi2/')
	save : bool, optional
		If True, will save the comparison images and ratios (the default is True)
	
	"""
	data = OrderedDict()
	data_header = ''
	chi2 = []
	chi2_header = ''
	get_fidelity = True
	if '.conv' in model_name:
		ref_name = 'model'
	elif '.sd' in model_name:
		ref_name = 'sd'
		get_fidelity = False
	else:
		ref_name = model_name

	for i, test in enumerate(tests):
		test_name = test.split('.')[-2]
		if save:
			r, c2 = compare(project, model_name, test, get_fidelity=get_fidelity,
				diag_name=d_base + '{}__{}_vs_{}.png'.format(model, ref_name, test_name), 
				results_name=r_base + '{}__{}_vs_{}.png'.format(model, ref_name, test_name))
		else:
			r, c2 = compare(project, model_name, test, get_fidelity=get_fidelity)

		if i == 0:
			data['uv'] = r['uv']
			data[ref_name] = r['pow_ref']
			data_header += 'uv,{},'.format(ref_name)
		data[test_name] = r['pow_im']
		data[test_name + '_ratio'] = r['ratio']
		data[test_name + '_ratio_err'] = r['err']
		data_header += '{0},{0}_ratio,{0}_ratio_err,'.format(test_name)
		chi2.append(c2)
		chi2_header += test_name


	chi2_header = 'feather,smclean,int,joint,tp2vis'
	if save:
		np.savetxt(data_base + '{}__{}.csv'.format(model, ref_name), 
			np.transpose(data.values()), header=data_header, delimiter=',')
		np.savetxt(chi2_base + '{}__{}-chi2.csv'.format(model, ref_name), chi2, 
			header=chi2_header, delimiter=',')



if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('project', help='The project folder.')
	parser.add_argument('-m', '--models', nargs='+', default=m.models.keys(),
		help='The models to run, out of {}'.format(m.models.keys()))
	parser.add_argument('-nm', '--not-models', nargs='+', default=[],
		help='The models to not run')
	parser.add_argument('--no-simulate', action='store_false', dest='simulate',  
		help='Only run comparison scrips (no simulation)')
	parser.add_argument('--compare-only', action='store_true')
	parser.add_argument('-p', '--parallel', action='store_true',
		help='run clean in parallel')
	parser.add_argument('-ns', '--no-save', action='store_false',
		help='Do not save plots and ratios on this run')
	args = parser.parse_args()

	models = [mod for mod in args.models if mod not in args.not_models]
	pipeline(args.project, models, args.simulate, args.compare_only, args.no_save, args.parallel)
